#!/bin/bash
exepath=$1

nthreads=$2
hashsize=$3

if [ "$4" == "e" ]
then
  elmo=1
  yane=0
elif [ "$4" == "y" ]
then
  elmo=0
  yane=1
elif [ "$4" == "ey" ]
then
  elmo=1
  yane=1
else
  elmo=0
  yane=0
fi

if [ "$5" != "" ]
then
  apery=$5
else
  apery=""
fi

# echo 'usi' | ${exepath} | awk -e '/id\ name/' -e '/id\ author/' | cut -d ' ' -f 3- > tmpfile
ename=`echo 'usi' | ${exepath} | grep 'id name' - | cut -d ' ' -f 3-`
eauthor=`echo 'usi' | ${exepath} | grep 'id author' - | cut -d ' ' -f 3-`
# mapfile -t params < tmpfile
# rm tmpfile

echo "    <Engine>"

if [ $elmo -eq 1 ]
then
  # echo "      <Name>${params[0]} elmo</Name>"
  echo "      <Name>$ename elmo</Name>"
else
  # echo "      <Name>${params[0]}</Name>"
  echo "      <Name>$ename</Name>"
fi

# echo "      <Author>${params[1]}</Author>"
echo "      <Author>$eauthor</Author>"
echo "      <Path>${exepath}</Path>"
echo "      <Encording>DEFAULT</Encording>"

echo "      <optionList>"

echo "        <option>"
echo "          <Key>Threads</Key>"
echo "          <value xsi:type=\"xsd:int\">$nthreads</value>"
echo "        </option>"

if [ $yane -eq 1 ]
then
  echo "        <option>"
  echo "          <Key>Hash</Key>"
  echo "          <value xsi:type=\"xsd:int\">$hashsize</value>"
  echo "        </option>"
else
  echo "        <option>"
  echo "          <Key>USI_Hash</Key>"
  echo "          <value xsi:type=\"xsd:int\">$hashsize</value>"
  echo "        </option>"
fi

if [ $elmo -eq 1 ]
then
  if [ "$apery" != "" ]
  then
    echo "        <option>"
    echo "          <Key>Book_File</Key>"
    echo "          <value xsi:type=\"xsd:string\">book/Apery_format/book.bin</value>"
    echo "        </option>"
    if [ $apery -eq 0 ]
    then
      echo "        <option>"
      echo "          <Key>Eval_Dir</Key>"
      echo "          <value xsi:type=\"xsd:string\">eval</value>"
      echo "        </option>"
    else
      if [ $apery -eq 1 ]
      then
        echo "        <option>"
        echo "          <Key>Eval_Dir</Key>"
        echo "          <value xsi:type=\"xsd:string\">eval_apery</value>"
        echo "        </option>"
      fi
    fi
  else
    echo "      <optionList/>"
  fi
else
  echo "      <optionList/>"
fi

echo "      </optionList>"

echo "    </Engine>"
